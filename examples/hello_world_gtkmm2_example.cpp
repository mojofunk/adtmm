#include <adtmm.hpp>

class HelloWorldWindow : public Gtk::Window
{
public:
	HelloWorldWindow()
		: m_button("Hello World")
	{

		m_button.signal_clicked().connect(sigc::mem_fun(*this, &HelloWorldWindow::on_button_clicked));
		add(m_button);

		m_button.show();
	}

private:

	void on_button_clicked()
	{
		A_CLASS_CALL();

		A_CLASS_MSG("Hello World");

		std::this_thread::sleep_for(std::chrono::seconds(1));

		A_CLASS_MSG("Goodbye World");
	}

private:
	Gtk::Button m_button;

	A_DECLARE_CLASS_MEMBERS(HelloWorldWindow);
};

A_DEFINE_CLASS_MEMBERS(HelloWorldWindow);

int main(int argc, char** argv)
{
	Gtk::Main kit(argc, argv);

	// must be created after Gtk::Main so DeveloperWindow is destroyed
	// before Gtk+ deinitialisation.
	adtmm::Application app(&argc, &argv);

	adt::Log::set_all_types_enabled(true);
	adt::Log::set_categories_enabled({ "HelloWorldWindow" }, true);
	adt::Log::get_sink()->set_enabled(true);
	adt::Log::set_trace_enabled();

	HelloWorldWindow window;

	adtmm::Application::get_developer_window()->present();

	Gtk::Main::run(window);

	return 0;
}
