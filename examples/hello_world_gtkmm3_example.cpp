#include <gtkmm.h>

#include "adtmm.hpp"

class ExampleWindow: public Gtk::ApplicationWindow
{
public:
	ExampleWindow()
		: m_button("Hello World")
	{
		A_CLASS_CALL();

		set_title("Gtk::Application adtmm example");

		m_button.signal_clicked().connect(sigc::mem_fun(*this, &ExampleWindow::on_button_clicked));
		add(m_button);

		m_button.show();
	}

	void on_button_clicked()
	{
		A_CLASS_CALL();

		A_CLASS_MSG("Hello World");

		std::this_thread::sleep_for(std::chrono::seconds(1));

		A_CLASS_MSG("Goodbye World");
	}

private:
	Gtk::Button m_button;

private:
	A_DECLARE_CLASS_MEMBERS(ExampleWindow);
};

A_DEFINE_CLASS_MEMBERS(ExampleWindow);

class ExampleApplication: public Gtk::Application
{
protected:
	ExampleApplication()
		: Gtk::Application("org.adtmm.examples.application")
	{
		Glib::set_application_name("Gtk::Application Example");
	}

public:
	static Glib::RefPtr<ExampleApplication> create()
	{
		return Glib::RefPtr<ExampleApplication>(new ExampleApplication());
	}

protected:
	//Overrides of default signal handlers:
	void on_activate()
	{
		A_CLASS_CALL();

		add_window(*adtmm::Application::get_developer_window());
		create_window();
	}

private:
	void create_window()
	{
		A_CLASS_CALL();

		auto window = new ExampleWindow();

		add_window(*window);

		window->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(*this,
		                              &ExampleApplication::on_window_hide), window));

		window->show();
	}

	void on_window_hide(Gtk::Window* window)
	{
		A_CLASS_CALL();

		delete window;
	}

private:
	A_DECLARE_CLASS_MEMBERS(ExampleApplication);
};

A_DEFINE_CLASS_MEMBERS(ExampleApplication);

int main(int argc, char *argv[])
{
	adtmm::Application app(&argc, &argv);

	// adt:: API can be used only before creating the DeveloperWindow
	// or GUI state won't match adt state.
	adt::Log::set_all_types_enabled();
	adt::Log::set_all_categories_enabled();
	adt::Log::get_sink()->set_enabled(true);
	adt::Log::set_trace_enabled();

	auto application = ExampleApplication::create();

	const int status = application->run(argc, argv);

	return status;
}
