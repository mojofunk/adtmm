Application::Application()
{
	ApplicationPrivate::get_instance()->set_log_config_from_library_options();
}

Application::Application(int* argc, char*** argv)
{
	ApplicationPrivate::get_instance()->parse_command_line(argc, argv);
}

Application::~Application()
{
	delete ApplicationPrivate::get_instance();
}

DeveloperWindow*
Application::get_developer_window()
{
	return ApplicationPrivate::get_instance()->get_developer_window();
}

void
Application::set_source_search_paths(std::vector<std::string> const& paths)
{
	ApplicationPrivate::get_instance()->set_source_search_paths(paths);
}

std::vector<std::string>
Application::get_source_search_paths()
{
	return ApplicationPrivate::get_instance()->get_source_search_paths();
}
