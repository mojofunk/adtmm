DeveloperWindow::DeveloperWindow()
    : d_ptr(new DeveloperWindowPrivate)
{
	set_title("Developer Tools");

	add(*d_ptr.get());

	set_default_size(800, 600);

	show_all();
}
