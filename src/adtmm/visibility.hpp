#ifdef ADTMM_STATIC

#define ADTMM_API
#define ADTMM_CAPI
#define ADTMM_LOCAL

#else // shared library is default

#ifdef _MSC_VER
#define ADTMM_EXPORT __declspec(dllexport)
#define ADTMM_IMPORT __declspec(dllimport)
#define ADTMM_LOCAL
#else
#define ADTMM_EXPORT __attribute__((visibility("default")))
#define ADTMM_IMPORT __attribute__((visibility("default")))
#define ADTMM_LOCAL __attribute__((visibility("hidden")))
#endif

#ifdef ADTMM_BUILDING_DLL
#define ADTMM_API ADTMM_EXPORT
#else
#define ADTMM_API ADTMM_IMPORT
#endif

#define ADTMM_CAPI extern "C" ADTMM_API

#endif
