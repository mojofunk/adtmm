class LibraryOptionsPrivate
{
public: // Data
	Glib::ustring log_categories;
	Glib::ustring record_types;
	std::string log_file;
	bool enable_trace = false;

	bool options_parsed = false;
};

LibraryOptions&
LibraryOptions::get_instance()
{
	static LibraryOptions options;
	return options;
}

LibraryOptions::LibraryOptions()
    : Glib::OptionGroup(
          "adtmm", _("adtmm options"), _("Command-line options for adtmm"))
    , d_ptr(new LibraryOptionsPrivate)
{
	Glib::OptionEntry entry;

	entry.set_long_name("log-categories");
	entry.set_short_name('L');
	entry.set_description(_(
	    "Set enabled log categories. Use \"-L list\" to see available options."));
	add_entry(entry, d_ptr->log_categories);

	entry.set_long_name("log-file");
	entry.set_short_name('l');
	entry.set_description(_("Set log output file."));
	add_entry_filename(entry, d_ptr->log_file);

	entry.set_long_name("log-enable-trace");
	entry.set_short_name('t');
	entry.set_description(_("Set stack tracing enabled by default for logging."));
	add_entry(entry, d_ptr->enable_trace);

	entry.set_long_name("log-record-types");
	entry.set_short_name('R');
	entry.set_description(
	    _("Set enabled log record types (all, message, call, data, object)."));
	add_entry(entry, d_ptr->record_types);
}

void
LibraryOptions::set_log_categories_string(const std::string& str)
{
	get_instance().d_ptr->log_categories = str;
}

std::string
LibraryOptions::get_log_categories_string()
{
	return get_instance().d_ptr->log_categories;
}

void
LibraryOptions::set_record_types_string(const std::string& str)
{
	get_instance().d_ptr->record_types = str;
}

std::string
LibraryOptions::get_record_types_string()
{
	return get_instance().d_ptr->record_types;
}

void
LibraryOptions::set_log_file_path(const std::string& file_path)
{
	get_instance().d_ptr->log_file = file_path;
}

std::string
LibraryOptions::get_log_file_path()
{
	return get_instance().d_ptr->log_file;
}

void
LibraryOptions::set_trace_enabled(bool enable)
{
	get_instance().d_ptr->enable_trace = enable;
}

bool
LibraryOptions::get_trace_enabled()
{
	return get_instance().d_ptr->enable_trace;
}

bool
LibraryOptions::parse_command_line(int* argc, char*** argv)
{
	if (get_instance().d_ptr->options_parsed) {
		// already been parsed by another OptionContext
		return true;
	}

	Glib::OptionContext context;

	context.set_main_group(get_instance());

	try {
		context.parse(*argc, *argv);
	} catch (const Glib::OptionError& ex) {
		std::cout << _("Error while parsing command-line options: ") << std::endl
		          << ex.what() << std::endl;
		std::cout << _("Use --help to see a list of available command-line options.")
		          << std::endl;
		return false;
	}

	catch (const Glib::Error& ex) {
		std::cout << "Error: " << ex.what() << std::endl;
		return false;
	}

	get_instance().d_ptr->options_parsed = true;

	return true;
}

void
LibraryOptions::enable_trace()
{
	adt::Log::set_trace_enabled(get_trace_enabled());
}

bool
LibraryOptions::enable_categories()
{
	auto const category_strings = split_string(get_log_categories_string(), ',');

	for (auto const& category_string : category_strings) {
		if (category_string == "list") {
			for (auto const& category : adt::Log::get_categories()) {
				std::cerr << category->name << std::endl;
			}
			exit(0);
		}

		if (category_string == "all") {
			adt::Log::set_all_categories_enabled(true);
		}
	}

	adt::Log::set_categories_enabled(category_strings, true);

	return category_strings.size();
}

void
LibraryOptions::enable_record_types()
{
	auto const type_strings = split_string(get_record_types_string(), ',');

	for (auto const& type_string : type_strings) {
		if (type_string == "all") {
			adt::Log::set_all_types_enabled(true);
			return;
		}

		if (type_string == "message") {
			adt::Log::set_message_type_enabled(true);
			continue;
		}

		if (type_string == "allocation") {
			adt::Log::set_allocation_type_enabled(true);
			continue;
		}

		if (type_string == "call") {
			adt::Log::set_function_type_enabled(true);
			continue;
		}

		if (type_string == "data") {
			adt::Log::set_data_type_enabled(true);
			continue;
		}
	}
}
