class LibraryOptionsPrivate;

class LibraryOptions : public Glib::OptionGroup
{
public:
	/**
	 * parse command line, removing adtmm relevant arguments
	 *
	 * Not intended to be public API, @see Application::parse_command_line
	 *
	 * @return true if successfully parsed.
	 */
	static bool parse_command_line(int* argc, char*** argv);

	/**
	 * The instance is required if an application wants to add the
	 * OptionGroup to the global option context
	 */
	static LibraryOptions& get_instance();

public:
	/* The set methods are only relevant if parse_options isn't called to
	 * set LibraryOption state, for instance if a different command line
	 * API was being used.
	 */
	static void set_log_categories_string(const std::string& categories);

	static std::string get_log_categories_string();

	static void set_record_types_string(const std::string& record_types);

	static std::string get_record_types_string();

	static void set_log_file_path(const std::string& file_path);

	static std::string get_log_file_path();

	static void set_trace_enabled(bool enable = true);

	static bool get_trace_enabled();

public: // Set adt::Log state based on option state
	static void enable_trace();

	static bool enable_categories();

	static void enable_record_types();

private:
	std::unique_ptr<LibraryOptionsPrivate> d_ptr;

private: // Ctors
	LibraryOptions();
};
