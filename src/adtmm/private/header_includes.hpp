#include <fcntl.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib/gstdio.h>

// For GlibFile
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#endif

#ifdef ADTMM_I18N_ENABLED
#include <glib/gi18n.h>
#else
#define _(String) String
#endif

#include <iostream>
#include <sstream>

#if defined(USE_GTK2) && defined(HAVE_GSV2)
#include <gtksourceviewmm.h>
#define USE_GSV
#define USE_GSV2
namespace Gsv
{
using View = gtksourceview::SourceView;
using Buffer = gtksourceview::SourceBuffer;
using Language = gtksourceview::SourceLanguage;
using LanguageManager = gtksourceview::SourceLanguageManager;
}
#endif

#if defined(USE_GTK3) && defined(HAVE_GSV3)
#include <gtksourceviewmm.h>
#define USE_GSV
#define USE_GSV3
#endif
