class ScopedSignalBlocker
{
public:
	ScopedSignalBlocker(SignalConnectionManager& manager)
	    : m_signal_connection_manager(manager)
	{
		m_signal_connection_manager.block();
	}

	~ScopedSignalBlocker() { m_signal_connection_manager.unblock(); }

private:
	SignalConnectionManager& m_signal_connection_manager;
};
