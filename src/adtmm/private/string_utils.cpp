std::string
thread_id_to_name(const std::thread::id& thread_id)
{
	auto thread_info = adt::Threads::get_thread_info(thread_id);

	std::ostringstream oss;

	if (thread_info) {
		oss << thread_info->name << "(" << thread_info->id << ")";
	} else {
		// not a registered thread
		oss << thread_id;
	}
	return oss.str();
}

std::string
thread_id_to_string(const std::thread::id& thread_id)
{
	std::ostringstream oss;
	oss << thread_id;
	return oss.str();
}

ThreadPriorityMap
thread_priority_map()
{
	return ThreadPriorityMap{ { "None", adt::ThreadPriority::NONE },
		                         { "Low", adt::ThreadPriority::LOW },
		                         { "Normal", adt::ThreadPriority::NORMAL },
		                         { "High", adt::ThreadPriority::HIGH },
		                         { "Realtime", adt::ThreadPriority::REALTIME } };
}

std::string
thread_priority_to_string(const adt::ThreadPriority& priority)
{
	for (auto const& p : thread_priority_map()) {
		if (p.second == priority) {
			return p.first;
		}
	}
	return std::string();
}

adt::ThreadPriority
string_to_thread_priority(const std::string& str)
{
	for (auto const& p : thread_priority_map()) {
		if (p.first == str) {
			return p.second;
		}
	}
	return adt::ThreadPriority::NONE;
}

std::string
instance_to_string(void* ptr)
{
	std::ostringstream oss;
	if (ptr != nullptr) {
		oss << ptr;
	} else {
		oss << "nullptr";
	}
	return oss.str();
}

std::vector<std::string>
split_string(const std::string& s, char delim)
{
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(std::move(item));
	}
	return elems;
}
