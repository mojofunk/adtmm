class SourceView : public Gtk::VBox
{
public:
	SourceView();

	void set_record(std::shared_ptr<adt::Record>& record);

	void set_source_file(const std::string& file_path);

	bool scroll_to_line(int line);

private:
	std::shared_ptr<adt::Record> m_record;

	Gtk::ScrolledWindow m_scrolled_window;

	Gtk::HBox m_top_hbox;

	Gtk::Label m_filename_label;

#ifdef USE_GSV
	Gsv::View m_source_view;
#else
	Gtk::TextView m_source_view;
#endif
};
