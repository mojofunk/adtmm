TraceView::TraceView()
{
	m_text_view.set_editable(false);

	m_scrolled_window.add(m_text_view);

	pack_start(m_scrolled_window);

	set_size_request(640, 480);
}

void
TraceView::set_trace(std::unique_ptr<adt::StackTrace> const& trace)
{
	auto buffer = m_text_view.get_buffer();

	if (!buffer) {
		ADTMM_DEBUG("TraceView::set_trace Unable to get buffer.");
		return;
	}

	if (!trace) {
		buffer->set_text("No Trace");
	} else {
		std::string trace_str = trace->to_string();
		buffer->set_text(trace_str);
	}
}

void
TraceView::scroll_to_line(int line)
{
	auto buffer = m_text_view.get_buffer();

	auto iter = buffer->get_iter_at_line(line);

	auto mark = buffer->create_mark(iter);

	m_text_view.scroll_to(mark, 0.0, 0.0, 0.3);

	// TODO highlight the line/create tag
}
