class ApplicationPrivate
{
public:
	static ApplicationPrivate* get_instance()
	{
		static ApplicationPrivate* s_instance = new ApplicationPrivate();
		return s_instance;
	}

private:
	ApplicationPrivate()
	    : m_update_timer(1000)
	    , m_max_records(4096)
	    , m_records_changed(false)
	    , m_sink(new adt::TraceEventSink)
	{
		set_auto_update(false);

		adt::Log::set_enabled();
		adt::Log::set_sink(m_sink);
		adt::Log::set_cache_enabled(m_max_records);

		m_update_timer.connect(
		    sigc::mem_fun(*this, &ApplicationPrivate::on_update_timer));

		g_log_set_writer_func(adtmm_log_writer, NULL, NULL);

#if defined(USE_GSV2)
		gtksourceview::init();
#elif defined(USE_GSV3)
		Gsv::init();
#endif
	}

public:
	~ApplicationPrivate()
	{
		// reinstate default writer function
		g_log_set_writer_func(g_log_writer_default, NULL, NULL);

		ADTMM_DEBUG("~ApplicationPrivate\n");
	}

public:
	const std::list<std::shared_ptr<adt::Record>>& get_records() const
	{
		return m_record_list;
	}

	uint32_t get_max_records() const { return m_max_records; }

	// TODO set max log records
	void set_max_records(uint32_t new_max) { m_max_records = new_max; }

	bool get_recording_enabled()
	{
		std::shared_ptr<adt::Sink> sink = adt::Log::get_sink();

		if (sink) {
			return sink->get_enabled();
		}
		return false;
	}

	void set_recording_enabled(bool enable)
	{
		std::shared_ptr<adt::Sink> sink = adt::Log::get_sink();
		sink->set_enabled(enable);
		m_recording_enabled_changed_signal();
	}

	void clear_records()
	{
		m_record_list.clear();
		m_records_changed = true;
		if (m_update_timer.suspended()) {
			on_update_timer();
		}
	}

	sigc::signal<void>& recording_enabled_changed_signal()
	{
		return m_recording_enabled_changed_signal;
	}

	sigc::signal<void>& records_changed_signal()
	{
		return m_records_changed_signal;
	}

public: // adt::Presets
	sigc::signal<void>& presets_changed_signal()
	{
		return m_presets_changed_signal;
	}

	void add_preset(std::shared_ptr<adt::Preset>);

	void remove_preset(std::shared_ptr<adt::Preset> const&);

	void set_preset_enabled(std::shared_ptr<adt::Preset> preset)
	{
		adt::Log::set_preset_enabled(preset);
		m_presets_changed_signal();
	}

	void set_preset_disabled(std::shared_ptr<adt::Preset> preset)
	{
		adt::Log::set_preset_disabled(preset);
		m_presets_changed_signal();
	}

public:
	void set_source_search_paths(std::vector<std::string> const& paths)
	{
		m_source_search_paths = paths;
	}

	std::vector<std::string> get_source_search_paths()
	{
		return m_source_search_paths;
	}

	void set_auto_update(bool enable)
	{
		if (enable && m_update_timer.enabled()) {
			return;
		}

		if (enable) {
			m_update_timer.resume();
		} else {
			m_update_timer.suspend();
		}
	}

	bool get_auto_update() const { return m_update_timer.enabled(); }

	sigc::connection auto_update_connect(const sigc::slot<void>& slot)
	{
		return m_update_timer.connect(slot);
	}

	uint32_t get_update_interval() const { return m_update_timer.get_interval(); }

	void set_update_interval(uint32_t msecs)
	{
		return m_update_timer.set_interval(msecs);
	}

public:
	bool parse_command_line(int* argc, char*** argv)
	{
		if (!LibraryOptions::parse_command_line(argc, argv)) {
			return false;
		}

		set_log_config_from_library_options();

		return true;
	}

	void set_log_config_from_library_options()
	{
		auto log_file_path = LibraryOptions::get_log_file_path();

		if (log_file_path.empty()) {
			// set a default output file path
			std::string default_log_filename = Glib::get_prgname() + ".trace.json";
			std::cout << "adtmm default output file: " << default_log_filename
			          << std::endl;
			log_file_path =
			    Glib::build_filename(Glib::get_home_dir(), default_log_filename);
		}

		std::shared_ptr<adt::File> log_file_sp(new GlibFile(log_file_path));
		m_sink->set_output_file(log_file_sp);

		LibraryOptions::enable_trace();

		if (LibraryOptions::enable_categories()) {
			m_sink->set_enabled(true);
		}

		LibraryOptions::enable_record_types();
	}

public:
	static std::vector<std::shared_ptr<LogCategory>> get_log_categories()
	{
		std::vector<std::shared_ptr<LogCategory>> categories;

		for (auto const& category : adt::Log::get_categories()) {
			categories.emplace_back(std::make_shared<AdtLogCategory>(category));
		}

#ifndef USE_GTK2
		categories.emplace_back(MAKE_GTK_LOG_CATEGORY(GTK_DEBUG_ACTIONS));
		categories.emplace_back(MAKE_GTK_LOG_CATEGORY(GTK_DEBUG_ICONTHEME));
#endif

		return categories;
	}

public:
	DeveloperWindow* get_developer_window()
	{
		if (!m_developer_window) {
			m_developer_window.reset(new DeveloperWindow);
		}
		return m_developer_window.get();
	}

public:
	void add_filter(std::shared_ptr<adt::Filter> filter)
	{
		adt::Log::add_filter(std::move(filter));
		m_filters_changed_signal();
	}

	void remove_filter(std::shared_ptr<adt::Filter> const& filter)
	{
		adt::Log::remove_filter(filter);
		m_filters_changed_signal();
	}

	sigc::signal<void>& filters_changed_signal()
	{
		return m_filters_changed_signal;
	}

private: // Methods
	void on_update_timer()
	{
		update_cache();

		if (m_records_changed) {
			// signal if records have changed since the last timer event
			m_records_changed_signal();
			m_records_changed = false;
		}
	}

	void update_cache()
	{
		auto const new_records = adt::Log::get_cached_records();

		for (auto const& new_record : new_records) {
			m_record_list.push_back(new_record);
			m_records_changed = true;

			while (m_record_list.size() > get_max_records()) {
				m_record_list.pop_front();
			}
		}
	}

private: // Data
	std::vector<std::string> m_source_search_paths;

	StandardTimer m_update_timer;

	uint32_t m_max_records;

	std::list<std::shared_ptr<adt::Record>> m_record_list;

	sigc::signal<void> m_recording_enabled_changed_signal;

	bool m_records_changed;
	sigc::signal<void> m_records_changed_signal;

	sigc::signal<void> m_filters_changed_signal;

	sigc::signal<void> m_presets_changed_signal;

	std::unique_ptr<DeveloperWindow> m_developer_window;

	std::shared_ptr<adt::TraceEventSink> m_sink;
};
