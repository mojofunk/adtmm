ThreadListView::ThreadListView()
    : m_add_filter_button("Add Filter")
{
	m_top_hbox.pack_start(m_add_filter_button, false, false);

	m_add_filter_button.set_sensitive(false);

	pack_start(m_top_hbox, Gtk::PACK_SHRINK);

	m_thread_list_store = Gtk::ListStore::create(m_rlcols);

	m_thread_tree_view.set_model(m_thread_list_store);
	m_thread_tree_view.append_column("Thread ID", m_rlcols.thread_id);
	m_thread_tree_view.append_column("Thread Name", m_rlcols.thread_name);
	m_thread_tree_view.append_column("Priority", m_rlcols.thread_priority);
	m_thread_tree_view.set_headers_visible(true);
	m_thread_tree_view.set_reorderable(false);
	m_thread_tree_view.get_selection()->set_mode(Gtk::SELECTION_SINGLE);

	auto column = m_thread_tree_view.get_column(0);
	if (column) {
		column->set_sort_column(m_rlcols.thread_id);
	}

	column = m_thread_tree_view.get_column(1);
	if (column) {
		column->set_sort_column(m_rlcols.thread_name);
	}

	column = m_thread_tree_view.get_column(2);
	if (column) {
		column->set_sort_column(m_rlcols.thread_priority);
	}

	m_thread_tree_view.get_selection()->signal_changed().connect(
	    sigc::mem_fun(this, &ThreadListView::on_selection_changed));

	m_scrolled_window.add(m_thread_tree_view);

	pack_start(m_scrolled_window);

	set_size_request(640, 480);

	ApplicationPrivate::get_instance()->auto_update_connect(
	    sigc::mem_fun(*this, &ThreadListView::on_auto_update));

	m_thread_tree_view.signal_row_activated().connect(
	    sigc::mem_fun(*this, &ThreadListView::on_row_activated));

	m_add_filter_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &ThreadListView::on_add_filter_button_pressed));
}

void
ThreadListView::on_selection_changed()
{
	Gtk::TreeModel::iterator i =
	    m_thread_tree_view.get_selection()->get_selected();

	if (i) {
		m_selected_thread_info = (*i)[m_rlcols.thread_info];
		m_add_filter_button.set_sensitive(true);
	} else {
		m_add_filter_button.set_sensitive(false);
	}
}

void
ThreadListView::on_add_filter_button_pressed()
{
	add_filter_from_selected();
}

void
ThreadListView::on_row_activated(const Gtk::TreeModel::Path&,
                                 Gtk::TreeViewColumn*)
{
	add_filter_from_selected();
}

void
ThreadListView::add_filter_from_selected()
{
	FilterDialog dialog("Add Filter", m_selected_thread_info);

	auto const response = dialog.run();

	if (response != Gtk::RESPONSE_OK) {
		return;
	}

	ApplicationPrivate::get_instance()->add_filter(dialog.get_filter());
}

void
ThreadListView::on_auto_update()
{
	refresh();
}

void
ThreadListView::refresh()
{
	std::vector<std::shared_ptr<adt::ThreadInfo>> all_thread_info;

	if (!adt::Threads::get_all_thread_info(all_thread_info)) {
		return;
	}

	// Add all new threads not already represented
	for (auto const& thread_info : all_thread_info) {
		bool found = false;

		for (auto const& iter : m_thread_list_store->children()) {
			auto const row = *iter;
			std::shared_ptr<adt::ThreadInfo> t_info = row[m_rlcols.thread_info];
			if (t_info == thread_info) {
				found = true;
				break;
			}
		}

		if (!found) {
			Gtk::TreeModel::Row newrow = *(m_thread_list_store)->append();
			newrow[m_rlcols.thread_id] = thread_id_to_string(thread_info->id);
			newrow[m_rlcols.thread_name] = thread_info->name;
			newrow[m_rlcols.thread_priority] =
			    thread_priority_to_string(thread_info->priority);
			newrow[m_rlcols.thread_info] = thread_info;
		}
	}

	// Remove all new threads that no longer exist
	for (auto iter = m_thread_list_store->children().begin();
	     iter != m_thread_list_store->children().end();) {
		auto const row = *iter;
		std::shared_ptr<adt::ThreadInfo> t_info = row[m_rlcols.thread_info];
		bool found = false;

		for (auto const& thread_info : all_thread_info) {
			if (t_info == thread_info) {
				found = true;
				break;
			}
		}

		if (!found) {
			iter = m_thread_list_store->erase(iter);
		} else {
			iter++;
		}
	}
}

void
ThreadListView::clear()
{
	m_thread_list_store->clear();
}
