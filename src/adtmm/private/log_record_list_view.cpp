LogRecordListView::LogRecordListView()
#ifndef USE_GTK3
    : m_clear_search_button("Clear Search")
    , m_add_filter_button("Add Filter")
#else
    : m_add_filter_button("Add Filter")
#endif
    , m_clear_records_button("Clear Records")
{
	m_top_hbox.pack_start(m_search_entry, true, true);

#ifndef USE_GTK3
	m_top_hbox.pack_start(m_clear_search_button, false, false);
#endif

	m_top_hbox.pack_start(m_clear_records_button, false, false);

	m_add_filter_button.set_sensitive(false);

	m_top_hbox.pack_start(m_add_filter_button, false, false);

	m_top_hbox.set_border_width(2);

	pack_start(m_top_hbox, Gtk::PACK_SHRINK);

	m_record_list_store = Gtk::ListStore::create(m_rlcols);

	m_tree_model_filter = Gtk::TreeModelFilter::create(m_record_list_store);

	m_tree_model_filter->set_visible_func(
	    sigc::mem_fun(*this, &LogRecordListView::is_row_visible));

	m_record_tree_view.set_model(m_tree_model_filter);
	m_record_tree_view.append_column("Category", m_rlcols.category);
	m_record_tree_view.append_column("Record Type", m_rlcols.record_type);
	m_record_tree_view.append_column("Message", m_rlcols.message);
	m_record_tree_view.append_column("Thread ID", m_rlcols.thread_id);
	m_record_tree_view.append_column("Duration(us)", m_rlcols.duration);
	m_record_tree_view.append_column("Function Name", m_rlcols.function_name);
	m_record_tree_view.append_column("File", m_rlcols.file_path);
	m_record_tree_view.set_headers_visible(true);
	m_record_tree_view.set_reorderable(false);
	m_record_tree_view.get_selection()->set_mode(Gtk::SELECTION_SINGLE);

	m_selection_changed_connection =
	    m_record_tree_view.get_selection()->signal_changed().connect(
	        sigc::mem_fun(this, &LogRecordListView::on_selection_changed));

	m_scrolled_window.add(m_record_tree_view);

	pack_start(m_scrolled_window);

	refresh();

#ifdef USE_GTK3
	m_search_entry.signal_search_changed().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_search_entry_changed));
#else
	m_search_entry.signal_changed().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_search_entry_changed));

	m_clear_search_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_clear_search_button_pressed));
#endif

	m_search_entry.signal_activate().connect(sigc::mem_fun(
	    *this, &LogRecordListView::on_search_entry_activated));

	m_search_entry.signal_key_press_event().connect(sigc::mem_fun(
	    *this, &LogRecordListView::on_search_entry_key_press_event));

	m_add_filter_button.signal_pressed().connect(sigc::mem_fun(
	    *this, &LogRecordListView::on_add_filter_button_pressed));

	m_clear_records_button.signal_clicked().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_clear_records_button_clicked));

	ApplicationPrivate::get_instance()->records_changed_signal().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_records_changed));

	m_record_tree_view.signal_row_activated().connect(
	    sigc::mem_fun(*this, &LogRecordListView::on_row_activated));
}

void
LogRecordListView::on_records_changed()
{
	refresh();
}

bool
LogRecordListView::is_row_visible(
    const Gtk::TreeModel::const_iterator& iter)
{
	auto row = *iter;
	return row[m_rlcols.visible];
}

void
LogRecordListView::on_selection_changed()
{
	Gtk::TreeModel::iterator i =
	    m_record_tree_view.get_selection()->get_selected();

	if (i) {
		m_selected_log_record = (*i)[m_rlcols.log_record];
		m_selected_log_record_changed_signal();
		m_add_filter_button.set_sensitive(true);
	} else {
		m_add_filter_button.set_sensitive(false);
	}
}

std::string
LogRecordListView::get_message_string_from_record(
    const adt::Record& record)
{
	std::string message_string;

	for (auto i = record.properties.begin(); i != record.properties.end();) {

		// these record types only use the first property
		if (record.type == adt::RecordType::MESSAGE ||
		    record.type == adt::RecordType::ALLOCATION) {
			message_string = i->value.to_string();
		} else {
			message_string += i->name + " : " + i->value.to_string();
		}

		auto const max_message_size = 120;

		if (message_string.length() > max_message_size) {
			message_string.resize(max_message_size);
			message_string += "...";
			break;
		}

		if (++i != record.properties.end()) {
			message_string += ", ";
		}
	}

	return message_string;
}

void
LogRecordListView::refresh()
{
	clear();

	for (auto const& record : ApplicationPrivate::get_instance()->get_records()) {
		Gtk::TreeModel::Row newrow = *(m_record_list_store)->append();
		newrow[m_rlcols.category] = record->category;
		newrow[m_rlcols.record_type] = adt::record_type_name(record->type);
		newrow[m_rlcols.message] = get_message_string_from_record(*record);
		newrow[m_rlcols.thread_id] = thread_id_to_name(record->thread_id);
		newrow[m_rlcols.duration] = record->timing.duration();
		newrow[m_rlcols.function_name] =
		    adt::short_function_name(record->src_location.function_name);
		newrow[m_rlcols.file_path] = record->src_location.file_path;
		newrow[m_rlcols.log_record] = record;
		newrow[m_rlcols.visible] = true;
	}
}

void
LogRecordListView::clear()
{
	m_selection_changed_connection.block();
	m_record_list_store->clear();
	m_selection_changed_connection.unblock();
	on_selection_changed();
}


bool
LogRecordListView::matches(const std::string& search_str,
                                  std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) {
		return std::tolower(c);
	});

	size_t found = str.find(search_str);

	if (found == std::string::npos) {
		return false;
	}
	return true;
}

void
LogRecordListView::on_search_entry_changed()
{
	std::string search_str = m_search_entry.get_text();

	std::transform(search_str.begin(),
	               search_str.end(),
	               search_str.begin(),
	               [](unsigned char c) { return std::tolower(c); });

	for (auto& child : m_record_list_store->children()) {
		// if (!child) continue;
		auto& row = *child;

		if (matches(search_str, row[m_rlcols.category]) ||
		    matches(search_str, row[m_rlcols.message]) ||
		    matches(search_str, row[m_rlcols.thread_id]) ||
		    matches(search_str, row[m_rlcols.function_name]) ||
		    matches(search_str, row[m_rlcols.file_path])) {
			row[m_rlcols.visible] = true;
		} else {
			row[m_rlcols.visible] = false;
		}
	}
}

void
LogRecordListView::on_search_entry_activated()
{
}

bool
LogRecordListView::on_search_entry_key_press_event(GdkEventKey* event)
{
	if (event->keyval == GDK_KEY_Escape) {
		m_search_entry.set_text("");
		return true;
	}
	return false;
}

void
LogRecordListView::on_clear_search_button_pressed()
{
	m_search_entry.set_text("");
}

void
LogRecordListView::on_clear_records_button_clicked()
{
	ApplicationPrivate::get_instance()->clear_records();
}

void
LogRecordListView::on_add_filter_button_pressed()
{
	add_filter_from_selected_record();
}

void
LogRecordListView::on_row_activated(const Gtk::TreeModel::Path&,
                                           Gtk::TreeViewColumn*)
{
	add_filter_from_selected_record();
}

void
LogRecordListView::add_filter_from_selected_record()
{
	FilterDialog dialog("Add Filter", m_selected_log_record);

	auto const response = dialog.run();

	if (response != Gtk::RESPONSE_OK) {
		return;
	}

	ApplicationPrivate::get_instance()->add_filter(dialog.get_filter());
}
