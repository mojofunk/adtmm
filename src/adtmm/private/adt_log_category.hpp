class AdtLogCategory : public LogCategory
{
public:
	AdtLogCategory(adt::LogCategory* cat)
	    : m_category(cat)
	{
	}

public:
	std::string get_name() override { return m_category->name; }

	void set_enabled(bool enable) override { m_category->enabled = enable; }

	bool get_enabled() override { return m_category->enabled; }

private:
	adt::LogCategory* m_category;
};
