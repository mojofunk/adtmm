GLogWriterOutput
adtmm_log_writer(GLogLevelFlags log_level,
                 const GLogField* fields,
                 gsize n_fields,
                 gpointer user_data);
