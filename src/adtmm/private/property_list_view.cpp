PropertyListView::PropertyListView()
{
	m_property_list_store = Gtk::ListStore::create(m_rlcols);

	m_property_tree_view.set_model(m_property_list_store);
	m_property_tree_view.append_column("Name", m_rlcols.property_name);
	m_property_tree_view.append_column("Value", m_rlcols.property_value);
	m_property_tree_view.append_column("Type", m_rlcols.property_type);
	m_property_tree_view.append_column("Function Name", m_rlcols.function_name);
	m_property_tree_view.set_headers_visible(true);
	m_property_tree_view.set_reorderable(false);
	m_property_tree_view.get_selection()->set_mode(Gtk::SELECTION_SINGLE);

	m_property_tree_view.get_selection()->signal_changed().connect(
	    sigc::mem_fun(this, &PropertyListView::on_selection_changed));

	m_scrolled_window.add(m_property_tree_view);

	pack_start(m_scrolled_window);

	set_size_request(640, 480);
}

void
PropertyListView::on_selection_changed()
{
	Gtk::TreeModel::iterator i =
	    m_property_tree_view.get_selection()->get_selected();

	if (i) {
		m_selected_property = (*i)[m_rlcols.property];
		m_selected_log_record = (*i)[m_rlcols.log_record];

		m_selected_property_changed_signal();
	}
}

void
PropertyListView::set_selected_log_record(std::shared_ptr<adt::Record> record)
{
	// if (clear_on_record_selection_change) {
	clear();

	for (auto const& property : record->properties) {
		Gtk::TreeModel::Row newrow = *(m_property_list_store)->append();
		newrow[m_rlcols.property_name] = property.name;
		newrow[m_rlcols.property_value] = property.value.to_string();
		newrow[m_rlcols.property_type] = adt::variant_type_name(property.value.type);
		newrow[m_rlcols.function_name] =
		    adt::short_function_name(record->src_location.function_name);
		newrow[m_rlcols.log_record] = record;
	}
}

void
PropertyListView::clear()
{
	m_property_list_store->clear();
}
