SourceView::SourceView()
{
	m_source_view.set_size_request(640, 480);
	m_source_view.set_editable(false);

#ifdef USE_GSV
	m_source_view.set_show_line_numbers();
	m_source_view.set_tab_width(4);
	m_source_view.set_auto_indent();
	// m_source_view.set_show_right_margin();
	// m_source_view.set_right_margin_position(80);
	m_source_view.set_highlight_current_line();
#ifdef USE_GSV2
	m_source_view.set_smart_home_end(gtksourceview::SOURCE_SMART_HOME_END_ALWAYS);
#else
	m_source_view.set_smart_home_end(Gsv::SMART_HOME_END_ALWAYS);
#endif

#endif

	m_scrolled_window.add(m_source_view);
	pack_start(m_scrolled_window);
}

void
SourceView::set_record(std::shared_ptr<adt::Record>& record)
{
	bool src_changed =
	    (!m_record ||
	     m_record->src_location.file_path != record->src_location.file_path);

	bool line_changed = (!m_record || src_changed ||
	                     m_record->src_location.line != record->src_location.line);

	m_record = record;

	if (src_changed) {
		std::string file_path = m_record->src_location.file_path;

		if (!Glib::path_is_absolute(file_path)) {
			if (!find_file_in_search_path(
			        ApplicationPrivate::get_instance()->get_source_search_paths(),
			        m_record->src_location.file_path,
			        file_path)) {
				ADTMM_DEBUG(
				    "SourceView::set_source_file Unable to find source file.\n");
			}
		}
		set_source_file(file_path);
	}

	if (line_changed) {
		// To correctly set scroll location when source/buffer changes an
		// idle handler must be used due to recalculations being performed
		// in an idle handler (which has already been scheduled).
		Glib::signal_idle().connect(
		    sigc::bind(sigc::mem_fun(*this, &SourceView::scroll_to_line),
		               m_record->src_location.line));
	}
}

void
SourceView::set_source_file(const std::string& file_path)
{
#ifdef USE_GSV
	Glib::RefPtr<Gsv::Buffer> buffer = m_source_view.get_source_buffer();
#else
	Glib::RefPtr<Gtk::TextBuffer> buffer = m_source_view.get_buffer();
#endif

	if (!buffer) {
		ADTMM_DEBUG(
		    "SourceView::set_source_file Unable to get source buffer.\n");
		return;
	}

	if (!Glib::file_test(file_path, Glib::FILE_TEST_EXISTS)) {
		buffer->set_text("Source File not Found");
		return;
	}

	std::string file_contents;
	try {
		file_contents = Glib::file_get_contents(file_path);
	} catch (Glib::FileError const& err) {
		ADTMM_DEBUG("SourceView::set_source_file ERROR: %s\n",
		            err.what().c_str());
		// set buffer text to empty below
	}

#ifdef USE_GSV
	buffer->begin_not_undoable_action();
	buffer->set_text(file_contents);
	buffer->end_not_undoable_action();

	Glib::RefPtr<Gsv::Language> language =
	    Gsv::LanguageManager::get_default()->guess_language(file_path,
	                                                        Glib::ustring());

	if (!language) {
		ADTMM_DEBUG(
		    "SourceView::set_source_file Unable to get source language.\n");
		return;
	}

	buffer->set_language(language);
#else // Gtk::TextView
	buffer->set_text(file_contents);
#endif
}

bool
SourceView::scroll_to_line(int line)
{
	Glib::RefPtr<Gtk::TextBuffer> buffer = m_source_view.get_buffer();

	Gtk::TextBuffer::iterator iter = buffer->get_iter_at_line(line);

	Glib::RefPtr<Gtk::TextBuffer::Mark> mark = buffer->create_mark(iter);

	m_source_view.scroll_to(mark, 0.0, 0.0, 0.3);

	// TODO highlight the line/create tag
	return false; // don't call again
}
