#ifdef ADTMM_ENABLE_DEBUG_OUTPUT
#include <cstdio>
#define ADTMM_DEBUG(...) printf(__VA_ARGS__)
#else
#define ADTMM_DEBUG(...)
#endif
