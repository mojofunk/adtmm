class GtkLogCategory : public LogCategory
{
public:
	GtkLogCategory(const char* const name, int bitmask)
	    : m_name(name)
	    , m_bitmask(bitmask)
	{
	}

public:
	std::string get_name() override { return m_name; }

	void set_enabled(bool enable) override
	{
		guint flags = gtk_get_debug_flags();

		if (enable) {
			flags |= m_bitmask;
		} else {
			flags &= ~m_bitmask;
		}
		gtk_set_debug_flags(flags);
	}

	bool get_enabled() override
	{
		guint flags = gtk_get_debug_flags();
		return flags & m_bitmask;
	}

private:
	const char* m_name;
	int m_bitmask;
};

#define MAKE_GTK_LOG_CATEGORY(Bitmask)                                         \
	std::make_shared<GtkLogCategory>(A_STRINGIFY(Bitmask), Bitmask)
