class TraceView : public Gtk::VBox
{
public:
	TraceView();

	void set_trace(std::unique_ptr<adt::StackTrace> const& trace);

	void scroll_to_line(int line);

private:
	Gtk::ScrolledWindow m_scrolled_window;
	Gtk::TextView m_text_view;
};
