class FilterListView : public Gtk::VBox
{
public:
	FilterListView();

	struct FilterListColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		FilterListColumns()
		{
			add(matching_text);
			add(thread_name);
			add(thread_priority);
			add(instance);
			add(file_path);
			add(function_name);
			add(filter);
		}
		Gtk::TreeModelColumn<std::string> matching_text;
		Gtk::TreeModelColumn<std::string> thread_name;
		Gtk::TreeModelColumn<std::string> thread_priority;
		Gtk::TreeModelColumn<std::string> instance;
		Gtk::TreeModelColumn<std::string> file_path;
		Gtk::TreeModelColumn<std::string> function_name;
		Gtk::TreeModelColumn<std::shared_ptr<adt::Filter>> filter;
	};

private: // Methods
	void clear();

	void on_selection_changed();

	void on_filters_changed();

	void on_row_activated(const Gtk::TreeModel::Path&, Gtk::TreeViewColumn*);

	void on_edit_filter_button_pressed();

	void edit_selected_filter();

	void on_remove_filter_button_pressed();

	void remove_selected_filter();

	void refresh();

private:
	Gtk::HBox m_top_hbox;

	Gtk::Button m_edit_filter_button;
	Gtk::Button m_remove_filter_button;

	Gtk::ScrolledWindow m_scrolled_window;

	Glib::RefPtr<Gtk::ListStore> m_filter_list_store;
	FilterListColumns m_rlcols;
	Gtk::TreeView m_filter_tree_view;

	std::shared_ptr<adt::Filter> m_selected_filter;
};
