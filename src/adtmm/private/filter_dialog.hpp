class FilterDialog : public Gtk::Dialog
{
public: // Ctors
	FilterDialog(const std::string& title,
	             const std::shared_ptr<adt::ThreadInfo>& thread_info)
	    : Dialog(title)
	{
		filter->thread_id = thread_info->id;
		filter->thread_priority = adt::Filter::get_thread_priority(filter->thread_id);

		filter->match_thread_id = true;

		init();
	}

	FilterDialog(const std::string& title,
	             const std::shared_ptr<adt::Record>& record)
	    : Dialog(title)
	{
		filter->thread_id = record->thread_id;
		filter->thread_priority = adt::Filter::get_thread_priority(filter->thread_id);
		filter->instance_ptr = record->this_ptr;
		filter->file_path = record->src_location.file_path;
		filter->function_name = record->src_location.function_name;

		init();
	}

	FilterDialog(const std::string& title,
	             const std::shared_ptr<adt::Filter>& p_filter)
	    : Dialog(title)
	{
		filter = p_filter;

		init();
	}

public: // methods
	void init()
	{
		fill_thread_priority_combobox();
		pack_table();
		set_widgets_from_filter();
		connect_signals();

		auto vbox = get_vbox();
		vbox->pack_start(table);
		vbox->show_all();

		add_button("Close", Gtk::RESPONSE_CLOSE);
		ok_button = add_button("OK", Gtk::RESPONSE_OK);
		ok_button->grab_focus();
		update_widget_sensitivity();
	}

	std::shared_ptr<adt::Filter> get_filter() { return filter; }

	void fill_thread_priority_combobox()
	{
		for (auto const& p : thread_priority_map()) {
			thread_priority_comboboxtext.append(p.first);
		}
	}

	void pack_table()
	{
		table.set_row_spacings(5);
		table.set_col_spacings(10);
		table.set_border_width(5);

		auto row_top = 0;
		auto row_bottom = 1;

		table.set_row_spacing(0, 20);
		table.attach(not_matching_checkbox, 0, 3, row_top, row_bottom);
		row_top++, row_bottom++;

		table.attach(match_thread_id_checkbox, 0, 1, row_top, row_bottom);
		table.attach(thread_id_label, 1, 3, row_top, row_bottom);
		row_top++, row_bottom++;

		table.attach(match_thread_priority_checkbox, 0, 1, row_top, row_bottom);
		table.attach(thread_priority_comboboxtext, 1, 3, row_top, row_bottom);
		row_top++, row_bottom++;

		table.attach(match_instance_checkbox, 0, 1, row_top, row_bottom);
		table.attach(instance_label, 1, 3, row_top, row_bottom);
		row_top++, row_bottom++;

		table.attach(match_file_path_checkbox, 0, 1, row_top, row_bottom);
		table.attach(file_path_label, 1, 3, row_top, row_bottom);
		row_top++, row_bottom++;

		table.attach(match_function_checkbox, 0, 1, row_top, row_bottom);
		table.attach(function_label, 1, 3, row_top, row_bottom);
	}

	void update_widget_sensitivity()
	{
		thread_id_label.set_sensitive(filter->match_thread_id);
		thread_priority_comboboxtext.set_sensitive(filter->match_thread_priority);
		instance_label.set_sensitive(filter->match_instance);
		file_path_label.set_sensitive(filter->match_file_path);
		function_label.set_sensitive(filter->match_function);

		ok_button->set_sensitive(filter->enabled());
	}

	void set_widgets_from_filter()
	{
		assert(filter);

		not_matching_checkbox.set_active(filter->not_matching);

		match_thread_id_checkbox.set_active(filter->match_thread_id);
		thread_id_label.set_text(thread_id_to_name(filter->thread_id));

		match_thread_priority_checkbox.set_active(filter->match_thread_priority);

		thread_priority_comboboxtext.set_active_text(
		    thread_priority_to_string(filter->thread_priority));

		match_instance_checkbox.set_active(filter->match_instance);
		if (filter->instance_ptr == nullptr) {
			match_instance_checkbox.set_sensitive(false);
		} else {
			instance_label.set_text(instance_to_string(filter->instance_ptr));
		}

		match_file_path_checkbox.set_active(filter->match_file_path);
		if (filter->file_path) {
			file_path_label.set_text(filter->file_path);
		} else {
			match_file_path_checkbox.hide();
			file_path_label.hide();
		}

		match_function_checkbox.set_active(filter->match_function);
		if (filter->function_name) {
			function_label.set_text(adt::short_function_name(filter->function_name));
		} else {
			match_function_checkbox.hide();
			function_label.hide();
		}
	}

	void update_filter_from_widgets()
	{
		filter->not_matching = not_matching_checkbox.get_active();

		filter->match_thread_id = match_thread_id_checkbox.get_active();

		filter->match_thread_priority = match_thread_priority_checkbox.get_active();
		auto priority_str = thread_priority_comboboxtext.get_active_text();
		filter->thread_priority = string_to_thread_priority(priority_str);

		filter->match_instance = match_instance_checkbox.get_active();
		filter->match_file_path = match_file_path_checkbox.get_active();
		filter->match_function = match_function_checkbox.get_active();

		update_widget_sensitivity();
	}

	void connect_signals()
	{
		not_matching_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		match_thread_id_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		match_thread_priority_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		thread_priority_comboboxtext.signal_changed().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		match_instance_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		match_file_path_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));

		match_function_checkbox.signal_toggled().connect(
		    sigc::mem_fun(*this, &FilterDialog::update_filter_from_widgets));
	}

public: // Data
	std::shared_ptr<adt::Filter> filter{ new adt::Filter() };

	Gtk::Button* ok_button{ nullptr };

	Gtk::Table table{ 7, 3 };

	Gtk::CheckButton not_matching_checkbox{ "Not Matching:" };

	Gtk::CheckButton match_thread_id_checkbox{ "Thread ID" };
	Gtk::Label thread_id_label;

	Gtk::CheckButton match_thread_priority_checkbox{ "Thread Priority" };
	Gtk::ComboBoxText thread_priority_comboboxtext;

	Gtk::CheckButton match_instance_checkbox{ "Instance" };
	Gtk::Label instance_label;

	Gtk::CheckButton match_file_path_checkbox{ "File Path" };
	Gtk::Label file_path_label;

	Gtk::CheckButton match_function_checkbox{ "Function" };
	Gtk::Label function_label;
};
