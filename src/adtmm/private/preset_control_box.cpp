PresetControlBox::PresetControlBox()
    : m_preset_label("Preset:")
    , m_preset_enable_button("Enable")
    , m_preset_disable_button("Disable")
{
	m_preset_list_store = Gtk::ListStore::create(m_preset_column_record);
	m_preset_combo_box.set_model(m_preset_list_store);
	m_preset_combo_box.pack_start(m_preset_column_record.name);

	refresh();

	pack_start(m_preset_label, false, false);
	pack_start(m_preset_combo_box);
	pack_start(m_preset_enable_button, false, false);
	pack_start(m_preset_disable_button, false, false);

	set_border_width(2);
	show_all();

	m_preset_enable_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &PresetControlBox::on_preset_enable_button_pressed));

	m_preset_disable_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &PresetControlBox::on_preset_disable_button_pressed));
}

void
PresetControlBox::refresh()
{
	m_preset_list_store->clear();

	for (auto const& preset : adt::Log::get_presets()) {
		Gtk::TreeModel::Row newrow = *(m_preset_list_store)->append();
		newrow[m_preset_column_record.name] = preset->name;
		newrow[m_preset_column_record.preset] = preset;
	}
	auto children = m_preset_list_store->children();

	if (!children.empty()) {
		m_preset_combo_box.set_active(*children.begin());
	}
}

void
PresetControlBox::on_preset_enable_button_pressed()
{
	auto const row = *m_preset_combo_box.get_active();

	if (row) {
		ApplicationPrivate::get_instance()->set_preset_enabled(
		    row[m_preset_column_record.preset]);
	}
}

void
PresetControlBox::on_preset_disable_button_pressed()
{
	auto const row = *m_preset_combo_box.get_active();

	if (row) {
		ApplicationPrivate::get_instance()->set_preset_disabled(
		    row[m_preset_column_record.preset]);
	}
}
