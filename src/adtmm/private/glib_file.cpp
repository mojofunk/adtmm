GlibFile::GlibFile(std::string const& file_path)
    : m_file_path(file_path)
    , m_file_fd(0)
{
}

GlibFile::~GlibFile()
{
	close();
}

bool
GlibFile::open()
{
	m_file_fd = ::g_open(m_file_path.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);

	if (m_file_fd < 0) {
		m_file_fd = 0;
		return false;
	}
	return true;
}

bool
GlibFile::close()
{
	if (m_file_fd) {
		if ((::close(m_file_fd) != 0)) {
			return false;
		}
		m_file_fd = 0;
	}
	return true;
}

bool
GlibFile::write(std::string const& str)
{
	return (::write(m_file_fd, str.c_str(), str.size()) != -1);
}
