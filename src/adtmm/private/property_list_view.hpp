class PropertyListView : public Gtk::VBox
{
public:
	PropertyListView();

	struct PropertyListColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		PropertyListColumns()
		{
			add(property_name);
			add(property_value);
			add(property_type);
			add(function_name);
			add(property);
			add(log_record);
		}
		Gtk::TreeModelColumn<std::string> property_name;
		Gtk::TreeModelColumn<std::string> property_value;
		Gtk::TreeModelColumn<std::string> property_type;
		Gtk::TreeModelColumn<std::string> function_name;
		Gtk::TreeModelColumn<adt::Property> property;
		Gtk::TreeModelColumn<std::shared_ptr<adt::Record>> log_record;
	};

	adt::Property const& get_selected_property() const
	{
		return m_selected_property;
	}

	std::shared_ptr<adt::Record> get_selected_log_record() const
	{
		return m_selected_log_record;
	}

	sigc::signal<void>& selected_property_changed()
	{
		return m_selected_property_changed_signal;
	}

	void set_selected_log_record(std::shared_ptr<adt::Record>);

private: // Methods
	void clear();

	void on_selection_changed();

private:
	Gtk::ScrolledWindow m_scrolled_window;

	Glib::RefPtr<Gtk::ListStore> m_property_list_store;
	PropertyListColumns m_rlcols;
	Gtk::TreeView m_property_tree_view;

	adt::Property m_selected_property;

	std::shared_ptr<adt::Record> m_selected_log_record;

	sigc::signal<void> m_selected_property_changed_signal;
};
