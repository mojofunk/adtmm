FilterListView::FilterListView()
    : m_edit_filter_button(Gtk::StockID("gtk-edit"))
    , m_remove_filter_button(Gtk::StockID("gtk-remove"))
{
	m_top_hbox.pack_start(m_edit_filter_button, false, false);

	m_top_hbox.pack_start(m_remove_filter_button, false, false);

	m_edit_filter_button.set_sensitive(false);
	m_remove_filter_button.set_sensitive(false);

	pack_start(m_top_hbox, Gtk::PACK_SHRINK);

	m_filter_list_store = Gtk::ListStore::create(m_rlcols);

	m_filter_tree_view.set_model(m_filter_list_store);
	m_filter_tree_view.append_column("Matching", m_rlcols.matching_text);
	m_filter_tree_view.append_column("Thread ID", m_rlcols.thread_name);
	m_filter_tree_view.append_column("Thread Priority", m_rlcols.thread_priority);
	m_filter_tree_view.append_column("Instance", m_rlcols.instance);
	m_filter_tree_view.append_column("File Path", m_rlcols.file_path);
	m_filter_tree_view.append_column("Function Name", m_rlcols.function_name);
	m_filter_tree_view.set_headers_visible(true);
	m_filter_tree_view.set_reorderable(false);
	m_filter_tree_view.get_selection()->set_mode(Gtk::SELECTION_SINGLE);

	m_filter_tree_view.get_selection()->signal_changed().connect(
	    sigc::mem_fun(this, &FilterListView::on_selection_changed));

	m_scrolled_window.add(m_filter_tree_view);

	pack_start(m_scrolled_window);

	set_size_request(640, 480);

	ApplicationPrivate::get_instance()->filters_changed_signal().connect(
	    sigc::mem_fun(*this, &FilterListView::on_filters_changed));

	m_filter_tree_view.signal_row_activated().connect(
	    sigc::mem_fun(*this, &FilterListView::on_row_activated));

	m_edit_filter_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &FilterListView::on_edit_filter_button_pressed));

	m_remove_filter_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &FilterListView::on_remove_filter_button_pressed));
}

void
FilterListView::on_selection_changed()
{
	Gtk::TreeModel::iterator i =
	    m_filter_tree_view.get_selection()->get_selected();

	if (i) {
		m_selected_filter = (*i)[m_rlcols.filter];
		m_edit_filter_button.set_sensitive(true);
		m_remove_filter_button.set_sensitive(true);
	} else {
		m_selected_filter.reset();
		m_edit_filter_button.set_sensitive(false);
		m_remove_filter_button.set_sensitive(false);
	}
}

void
FilterListView::on_edit_filter_button_pressed()
{
	edit_selected_filter();
}

void
FilterListView::on_remove_filter_button_pressed()
{
	remove_selected_filter();
}

void
FilterListView::on_row_activated(const Gtk::TreeModel::Path&,
                                 Gtk::TreeViewColumn*)
{
	edit_selected_filter();
}

void
FilterListView::edit_selected_filter()
{
	FilterDialog dialog("Edit Filter", m_selected_filter);

	auto const response = dialog.run();

	if (response != Gtk::RESPONSE_OK) {
		return;
	}

	// not sure about this design, seems wrong
	ApplicationPrivate::get_instance()->remove_filter(m_selected_filter);
	ApplicationPrivate::get_instance()->add_filter(dialog.get_filter());
}

void
FilterListView::remove_selected_filter()
{
	ApplicationPrivate::get_instance()->remove_filter(m_selected_filter);
}

void
FilterListView::on_filters_changed()
{
	refresh();
}

void
FilterListView::refresh()
{
	clear();

	Gtk::TreeModel::Row newrow;
	for (auto const& filter : adt::Log::get_filters()) {
		newrow = *(m_filter_list_store)->append();
		if (filter->not_matching) {
			newrow[m_rlcols.matching_text] = "Not Matching";
		} else {
			newrow[m_rlcols.matching_text] = "Matching";
		}
		if (filter->match_thread_id) {
			newrow[m_rlcols.thread_name] = thread_id_to_name(filter->thread_id);
		}
		if (filter->match_thread_priority) {
			newrow[m_rlcols.thread_priority] =
			    thread_priority_to_string(filter->thread_priority);
		}
		if (filter->match_instance) {
			newrow[m_rlcols.instance] = instance_to_string(filter->instance_ptr);
		}
		if (filter->match_file_path) {
			newrow[m_rlcols.file_path] = filter->file_path;
		}
		if (filter->match_function) {
			newrow[m_rlcols.function_name] =
			    adt::short_function_name(filter->function_name);
		}
		newrow[m_rlcols.filter] = filter;
	}

	if (newrow) {
		m_filter_tree_view.get_selection()->select(newrow);
	}
}

void
FilterListView::clear()
{
	m_filter_list_store->clear();
}
