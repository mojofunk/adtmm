class ThreadListView : public Gtk::VBox
{
public:
	ThreadListView();

	struct ThreadListColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		ThreadListColumns()
		{
			add(thread_id);
			add(thread_name);
			add(thread_priority);
			add(thread_info);
		}
		Gtk::TreeModelColumn<std::string> thread_id;
		Gtk::TreeModelColumn<std::string> thread_name;
		Gtk::TreeModelColumn<std::string> thread_priority;
		Gtk::TreeModelColumn<std::shared_ptr<adt::ThreadInfo>> thread_info;
	};

private: // Methods
	void refresh();

	void clear();

	void on_selection_changed();

	void on_auto_update();

	void on_row_activated(const Gtk::TreeModel::Path&, Gtk::TreeViewColumn*);

	void on_add_filter_button_pressed();

	void add_filter_from_selected();

private:
	Gtk::HBox m_top_hbox;

	Gtk::Button m_add_filter_button;

	Gtk::ScrolledWindow m_scrolled_window;

	Glib::RefPtr<Gtk::ListStore> m_thread_list_store;
	ThreadListColumns m_rlcols;
	Gtk::TreeView m_thread_tree_view;

	std::shared_ptr<adt::ThreadInfo> m_selected_thread_info;
};
