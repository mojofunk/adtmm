class PresetControlBox : public Gtk::HBox
{
public:
	PresetControlBox();

private: // Classes
	struct PresetColumnRecord : public Gtk::TreeModel::ColumnRecord {
		PresetColumnRecord()
		{
			add(name);
			add(preset);
		}
		Gtk::TreeModelColumn<std::string> name;
		Gtk::TreeModelColumn<std::shared_ptr<adt::Preset>> preset;
	};

private: // Methods
	void refresh();

private: // Event handlers
	void on_preset_enable_button_pressed();
	void on_preset_disable_button_pressed();

private: // Members
	Gtk::Label m_preset_label;
	Gtk::Button m_preset_enable_button;
	Gtk::Button m_preset_disable_button;

	Glib::RefPtr<Gtk::ListStore> m_preset_list_store;
	PresetColumnRecord m_preset_column_record;
	Gtk::ComboBox m_preset_combo_box;
};
