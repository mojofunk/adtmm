DeveloperWindowPrivate::DeveloperWindowPrivate()
{
	m_top_hpaned.pack1(log_category_control, false, false);
	m_top_hpaned.pack2(log_record_list_view, true, true);

	m_top_hbox.pack_start(m_top_hpaned, true, true);

	m_bottom_left_notebook.append_page(source_view, "Source View");
	m_bottom_left_notebook.append_page(filter_view, "Filter List");
	m_bottom_left_notebook.append_page(thread_view, "Thread List");
	m_bottom_left_notebook.append_page(class_list_view, "Class List");

	m_bottom_right_notebook.append_page(trace_view, "Trace View");
	m_bottom_right_notebook.append_page(property_list_view, "Property List");

	m_bottom_hpaned.pack1(m_bottom_left_notebook, true, true);
	m_bottom_hpaned.pack2(m_bottom_right_notebook, true, true);

	m_vpaned.pack1(m_top_hbox, true, true);
	m_vpaned.pack2(m_bottom_hpaned, true, true);

	pack_start(log_control, false, false);
	pack_start(m_vpaned);

	set_border_width(2);

	log_record_list_view.selected_record_changed().connect(
	    sigc::mem_fun(*this, &DeveloperWindowPrivate::on_selected_log_record_changed));
}

void
DeveloperWindowPrivate::on_selected_log_record_changed()
{
	auto log_record = log_record_list_view.get_selected_log_record();

	source_view.set_record(log_record);

	trace_view.set_trace(log_record->stack_trace);

	property_list_view.set_selected_log_record(log_record);
}
