class LogRecordListView : public Gtk::VBox
{
public:
	LogRecordListView();

	struct LogRecordListColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		LogRecordListColumns()
		{
			add(category);
			add(record_type);
			add(message);
			add(thread_id);
			add(duration);
			add(function_name);
			add(file_path);
			add(log_record);
			add(visible);
		}
		Gtk::TreeModelColumn<std::string> category;
		Gtk::TreeModelColumn<std::string> record_type;
		Gtk::TreeModelColumn<std::string> message;
		Gtk::TreeModelColumn<std::string> thread_id;
		Gtk::TreeModelColumn<uint64_t> duration;
		Gtk::TreeModelColumn<std::string> function_name;
		Gtk::TreeModelColumn<std::string> file_path;
		Gtk::TreeModelColumn<std::shared_ptr<adt::Record>> log_record;
		Gtk::TreeModelColumn<bool> visible;
	};

	std::shared_ptr<adt::Record> get_selected_log_record() const
	{
		return m_selected_log_record;
	}

	sigc::signal<void>& selected_record_changed()
	{
		return m_selected_log_record_changed_signal;
	}

private: // Methods
	void clear();
	void refresh();

	std::string get_thread_id_string(const std::thread::id&) const;

	std::string get_message_string_from_record(const adt::Record& record);

	bool is_row_visible(const Gtk::TreeModel::const_iterator& iter);

	void on_records_changed();

	void on_selection_changed();

	bool matches(const std::string& search_str, std::string str);

	void on_search_entry_changed();
	void on_search_entry_activated();
	bool on_search_entry_key_press_event(GdkEventKey*);

	void on_clear_search_button_pressed();

	void on_clear_records_button_clicked();

	void on_row_activated(const Gtk::TreeModel::Path&, Gtk::TreeViewColumn*);

	void on_add_filter_button_pressed();

	void add_filter_from_selected_record();

private: // Data
	Gtk::HBox m_top_hbox;

#ifdef USE_GTK3
	Gtk::SearchEntry m_search_entry;
#else
	Gtk::Entry m_search_entry;
	Gtk::Button m_clear_search_button;
#endif

	Gtk::Button m_add_filter_button;

	Gtk::Button m_clear_records_button;

	Gtk::ScrolledWindow m_scrolled_window;

	Glib::RefPtr<Gtk::ListStore> m_record_list_store;
	LogRecordListColumns m_rlcols;
	Glib::RefPtr<Gtk::TreeModelFilter> m_tree_model_filter;

	Gtk::TreeView m_record_tree_view;

	std::shared_ptr<adt::Record> m_selected_log_record;

	sigc::connection m_selection_changed_connection;
	sigc::signal<void> m_selected_log_record_changed_signal;
};
