LogCategoryControl::LogCategoryControl()
#ifndef USE_GTK3
    : m_clear_button(Gtk::StockID("gtk-clear"))
#endif
{
	m_search_box.pack_start(m_search_entry);

#ifndef USE_GTK3
	m_search_box.pack_start(m_clear_button);
#endif

	m_search_box.set_border_width(2);

	pack_start(m_search_box, Gtk::PACK_SHRINK);

	m_category_list_store = Gtk::ListStore::create(m_llcols);
	m_category_list_store->set_sort_column(m_llcols.name, Gtk::SORT_ASCENDING);

	// Put the TreeModel inside a filter model
	m_tree_model_filter = Gtk::TreeModelFilter::create(m_category_list_store);

	m_tree_model_filter->set_visible_func(
	    sigc::mem_fun(*this, &LogCategoryControl::is_row_visible));

	category_tree_view.set_model(m_tree_model_filter);
	category_tree_view.append_column("Category Name", m_llcols.name);
	category_tree_view.append_column_editable("Enable", m_llcols.enabled);
	category_tree_view.set_headers_visible(true);
	category_tree_view.set_reorderable(false);
	category_tree_view.get_selection()->set_mode(Gtk::SELECTION_NONE);

	Gtk::CellRendererToggle* toggle = dynamic_cast<Gtk::CellRendererToggle*>(
	    category_tree_view.get_column_cell_renderer(1));

	toggle->signal_toggled().connect(sigc::mem_fun(
	    *this, &LogCategoryControl::category_enabled_toggled));

	m_scrolled_window.add(category_tree_view);

	pack_start(m_scrolled_window);

	pack_start(m_preset_control_box, false, false);

	set_size_request(400, 400);

	refresh();

#ifdef USE_GTK3
	m_search_entry.signal_search_changed().connect(
	    sigc::mem_fun(*this, &LogCategoryControl::on_search_entry_changed));
#else
	m_search_entry.signal_changed().connect(
	    sigc::mem_fun(*this, &LogCategoryControl::on_search_entry_changed));

	m_clear_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &LogCategoryControl::on_clear_button_pressed));
#endif

	m_search_entry.signal_activate().connect(sigc::mem_fun(
	    *this, &LogCategoryControl::on_search_entry_activated));

	m_search_entry.signal_key_press_event().connect(sigc::mem_fun(
	    *this, &LogCategoryControl::on_search_entry_key_press_event));

	ApplicationPrivate::get_instance()->presets_changed_signal().connect(
	    sigc::mem_fun(*this, &LogCategoryControl::refresh));
}

void
LogCategoryControl::refresh()
{
	m_category_list_store->clear();

	auto const categories =
	    ApplicationPrivate::get_instance()->get_log_categories();

	for (auto const& category : categories) {
		Gtk::TreeModel::Row newrow = *(m_category_list_store)->append();
		newrow[m_llcols.name] = category->get_name();
		newrow[m_llcols.enabled] = category->get_enabled();
		newrow[m_llcols.category] = category;
		newrow[m_llcols.visible] = true;
	}
}

void
LogCategoryControl::category_enabled_toggled(const Glib::ustring& path)
{
	Gtk::TreeModel::iterator iter = m_category_list_store->get_iter(path);
	bool enabled = iter->get_value(m_llcols.enabled);
	auto category = iter->get_value(m_llcols.category);

	assert(category->get_enabled() != enabled);

	category->set_enabled(enabled);
}

bool
LogCategoryControl::is_row_visible(
    const Gtk::TreeModel::const_iterator& iter)
{
	auto row = *iter;
	return row[m_llcols.visible];
}

void
LogCategoryControl::on_search_entry_changed()
{
	std::string search_str = m_search_entry.get_text();

	std::transform(search_str.begin(),
	               search_str.end(),
	               search_str.begin(),
	               [](unsigned char c) { return std::tolower(c); });

	for (auto& child : m_category_list_store->children()) {
		if (child) {
			auto& row = *child;
			std::string category_name = row[m_llcols.name];

			std::transform(category_name.begin(),
			               category_name.end(),
			               category_name.begin(),
			               [](unsigned char c) { return std::tolower(c); });

			size_t found = category_name.find(search_str);

			if (found == std::string::npos) {
				row[m_llcols.visible] = false;
			} else {
				row[m_llcols.visible] = true;
			}
		}
	}
}

void
LogCategoryControl::on_search_entry_activated()
{
	bool first_row = true;
	bool enable = false;

	for (auto& child : m_category_list_store->children()) {
		if (child) {
			auto& row = *child;

			// toggle all visible based on enabled state of first row
			if (row[m_llcols.visible] == true) {
				if (first_row) {
					first_row = false;
					enable = !row[m_llcols.enabled];
				}
				row[m_llcols.enabled] = enable;

				auto category = row.get_value(m_llcols.category);

				category->set_enabled(enable);
			}
		}
	}
}

bool
LogCategoryControl::on_search_entry_key_press_event(GdkEventKey* event)
{
	if (event->keyval == GDK_KEY_Escape) {
		m_search_entry.set_text("");
		return true;
	}
	return false;
}

void
LogCategoryControl::on_clear_button_pressed()
{
	m_search_entry.set_text("");
}
