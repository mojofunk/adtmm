std::string
thread_id_to_name(const std::thread::id&);

std::string
thread_id_to_string(const std::thread::id&);

using ThreadPriorityMap = std::map<std::string, adt::ThreadPriority>;

ThreadPriorityMap
thread_priority_map();

std::string
thread_priority_to_string(const adt::ThreadPriority&);

adt::ThreadPriority
string_to_thread_priority(const std::string&);

std::string
instance_to_string(void* ptr);

std::vector<std::string>
split_string(const std::string& s, char delim);
