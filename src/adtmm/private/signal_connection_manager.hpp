class SignalConnectionManager
{
public:
	void block()
	{
		for (auto& connection : connections) {
			connection.block();
		}
	}

	void unblock()
	{
		for (auto& connection : connections) {
			connection.unblock();
		}
	}

	std::vector<sigc::connection> connections;
};
