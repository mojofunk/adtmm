A_DEFINE_LOG_CATEGORY(GlibError, "Glib::ERROR");
A_DEFINE_LOG_CATEGORY(GlibCritical, "Glib::CRITICAL");
A_DEFINE_LOG_CATEGORY(GlibWarning, "Glib::WARNING");
A_DEFINE_LOG_CATEGORY(GlibMessage, "Glib::MESSAGE");
A_DEFINE_LOG_CATEGORY(GlibInfo, "Glib::INFO");
A_DEFINE_LOG_CATEGORY(GlibDebug, "Glib::DEBUG");

adt::LogCategory*
log_level_to_log_category(GLogLevelFlags log_level)
{
	switch (log_level & G_LOG_LEVEL_MASK) {
	case G_LOG_LEVEL_DEBUG:
		return GlibDebug;
	case G_LOG_LEVEL_MESSAGE:
		return GlibMessage;
	case G_LOG_LEVEL_INFO:
		return GlibInfo;
	case G_LOG_LEVEL_CRITICAL:
		return GlibCritical;
	case G_LOG_LEVEL_WARNING:
		return GlibWarning;
	}
	return GlibError;
}

GLogWriterOutput
adtmm_log_writer(GLogLevelFlags log_level,
                 const GLogField* fields,
                 gsize n_fields,
                 gpointer user_data)
{
	auto log_category = log_level_to_log_category(log_level);

	if (!log_category->enabled || !log_category->message_type_enabled) {
		return G_LOG_WRITER_UNHANDLED;
	}

	const char* message_str = nullptr;
	const char* file_str = nullptr;
	const char* line_str = nullptr;
	const char* func_str = nullptr;
	const char* domain_str = nullptr;

	for (gsize i = 0; i < n_fields; i++) {
		const GLogField* field = &fields[i];

		if (field->length != -1) {
			continue;
		}

		if (strcmp(field->key, "MESSAGE") == 0) {
			message_str = (const char*)field->value;
			continue;
		}

		if (strcmp(field->key, "CODE_FILE") == 0) {
			file_str = (const char*)field->value;
			continue;
		}

		if (strcmp(field->key, "CODE_LINE") == 0) {
			line_str = (const char*)field->value;
			continue;
		}

		if (strcmp(field->key, "CODE_FUNC") == 0) {
			func_str = (const char*)field->value;
			continue;
		}

		if (strcmp(field->key, "GLIB_DOMAIN") == 0) {
			domain_str = (const char*)field->value;
			continue;
		}
	}

	if (!message_str) {
		message_str = "Glib MESSAGE field empty...this should not occur!";
	}

	int line = 0;

	if (line_str) {
		std::istringstream iss(line_str);
		iss.imbue(std::locale::classic());
		iss >> line;
	}

	if (!file_str) {
		file_str = __FILE__;
	}

	if (!func_str) {
		func_str = A_STRFUNC;
	}

	if (!domain_str) {
		domain_str = "None";
	}

	adt::MessageLogger logger(log_category, line, file_str, func_str);

	if (logger.get_enabled()) {
		// copy/move are the same with char pointer types, both result in an
		// adt::String copying the string. Which is good as it is about to
		// be set free().
		logger.log_record->move_to_property_1("Domain", domain_str);
		logger.log_record->move_to_property_2(adt::record_type_message_string,
		                                      message_str);
	}

	// TODO StackTrace

	return G_LOG_WRITER_HANDLED;
}
