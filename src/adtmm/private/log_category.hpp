class LogCategory
{
public:
	virtual ~LogCategory() {}

public:
	virtual std::string get_name() = 0;

	virtual void set_enabled(bool enable) = 0;

	virtual bool get_enabled() = 0;
};
