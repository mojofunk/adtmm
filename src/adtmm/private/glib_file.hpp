class GlibFile : public adt::File
{
public:
	GlibFile(std::string const& file_path);

	~GlibFile() override;

public: // adt::File interface
	bool open() override;

	bool close() override;

	bool write(std::string const& str) override;

private: // Data
	std::string const m_file_path;
	int m_file_fd;
};
