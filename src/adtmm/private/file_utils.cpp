bool
find_file_in_search_path(std::vector<std::string> const& paths,
                         std::string const& filename,
                         std::string& absolute_file_path)
{
	for (auto const& path : paths) {
		absolute_file_path = Glib::build_filename(path, filename);
		if (Glib::file_test(absolute_file_path,
		                    Glib::FILE_TEST_EXISTS | Glib::FILE_TEST_IS_REGULAR)) {
			return true;
		}
	}
	return false;
}
