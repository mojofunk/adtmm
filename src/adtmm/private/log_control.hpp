class LogControl : public Gtk::HBox
{
public:
	LogControl();

	void update_button_state();

private: // Methods
	void on_recording_button_toggled();
	void on_auto_update_button_toggled();

	void on_sink_recording_enabled_changed();

	void on_filters_changed();

	void on_enable_tracing_button_toggled();
	void on_enable_sync_button_toggled();
	void on_enable_properties_button_toggled();
	void on_enable_filters_button_toggled();

	void on_enable_messages_button_toggled();
	void on_enable_function_calls_button_toggled();
	void on_enable_data_button_toggled();
	void on_enable_allocation_button_toggled();

	void on_enable_global_allocation_button_toggled();

	void update_recording_button_state();

private: // Data
	Gtk::HBox m_button_hbox;

	Gtk::ToggleButton m_recording_button;

	Gtk::ToggleButton m_auto_update_button;

	Gtk::HButtonBox m_options_button_hbox;

	Gtk::ToggleButton m_enable_tracing_button;

	Gtk::ToggleButton m_enable_sync_button;

	Gtk::ToggleButton m_enable_properties_button;

	Gtk::ToggleButton m_enable_filters_button;

	Gtk::HButtonBox m_record_types_button_hbox;

	Gtk::ToggleButton m_enable_messages_button;

	Gtk::ToggleButton m_enable_function_calls_button;

	Gtk::ToggleButton m_enable_data_button;

	Gtk::ToggleButton m_enable_allocation_button;

	Gtk::ToggleButton m_enable_global_allocation_button;

	// Add Record count Label
};
