LogControl::LogControl()
    : m_recording_button(Gtk::StockID("gtk-media-record"))
    , m_auto_update_button("Auto Update")
    , m_enable_tracing_button("Stack Traces")
    , m_enable_sync_button("Sync")
    , m_enable_properties_button("Properties")
    , m_enable_filters_button("Filters")
    , m_enable_messages_button("Messages")
    , m_enable_function_calls_button("Function Calls")
    , m_enable_data_button("Data")
    , m_enable_allocation_button("Class Allocation")
    , m_enable_global_allocation_button("Global Allocation")
{
	m_button_hbox.pack_start(m_recording_button, true, true);
	m_button_hbox.pack_start(m_auto_update_button, true, true);
	m_button_hbox.set_spacing(5);

	pack_start(m_button_hbox, false, false);

	m_enable_tracing_button.set_active(adt::Log::get_trace_enabled());
	m_options_button_hbox.pack_start(m_enable_tracing_button, true, true);

	m_enable_sync_button.set_active(adt::Log::get_sync_enabled());
	m_options_button_hbox.pack_start(m_enable_sync_button, true, true);

	m_enable_properties_button.set_active(adt::Log::get_properties_enabled());
	m_options_button_hbox.pack_start(m_enable_properties_button, true, true);

	m_enable_filters_button.set_active(adt::Log::get_filters_enabled());
	m_options_button_hbox.pack_start(m_enable_filters_button, true, true);

	m_options_button_hbox.set_spacing(5);

	pack_start(m_options_button_hbox, false, false);

	m_enable_messages_button.set_active(adt::Log::get_message_type_enabled());
	m_enable_function_calls_button.set_active(
	    adt::Log::get_function_type_enabled());
	m_enable_data_button.set_active(adt::Log::get_data_type_enabled());
	m_enable_allocation_button.set_active(adt::Log::get_allocation_type_enabled());

	m_enable_global_allocation_button.set_active(
	    adt::GlobalAllocator::get_logging_enabled());

	m_record_types_button_hbox.pack_start(m_enable_messages_button, true, true);
	m_record_types_button_hbox.pack_start(
	    m_enable_function_calls_button, true, true);
	m_record_types_button_hbox.pack_start(m_enable_data_button, true, true);
	m_record_types_button_hbox.pack_start(m_enable_allocation_button, true, true);
	m_record_types_button_hbox.pack_start(
	    m_enable_global_allocation_button, true, true);
	m_record_types_button_hbox.set_spacing(5);

	pack_start(m_record_types_button_hbox, false, false);

	set_spacing(10);

	m_auto_update_button.set_active(
	    ApplicationPrivate::get_instance()->get_auto_update());

	// Connect to sink signals

	ApplicationPrivate::get_instance()->recording_enabled_changed_signal().connect(
	    sigc::mem_fun(*this, &LogControl::on_sink_recording_enabled_changed));

	ApplicationPrivate::get_instance()->filters_changed_signal().connect(
	    sigc::mem_fun(*this, &LogControl::on_filters_changed));

	ApplicationPrivate::get_instance()->presets_changed_signal().connect(
	    sigc::mem_fun(*this, &LogControl::update_button_state));

	// Connect to button signals

	m_recording_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_recording_button_toggled));

	m_auto_update_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_auto_update_button_toggled));

	m_enable_tracing_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_tracing_button_toggled));

	m_enable_sync_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_sync_button_toggled));

	m_enable_properties_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_properties_button_toggled));

	m_enable_filters_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_filters_button_toggled));

	m_enable_messages_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_messages_button_toggled));

	m_enable_function_calls_button.signal_toggled().connect(sigc::mem_fun(
	    *this, &LogControl::on_enable_function_calls_button_toggled));

	m_enable_data_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_data_button_toggled));

	m_enable_allocation_button.signal_toggled().connect(
	    sigc::mem_fun(*this, &LogControl::on_enable_allocation_button_toggled));

	m_enable_global_allocation_button.signal_toggled().connect(sigc::mem_fun(
	    *this, &LogControl::on_enable_global_allocation_button_toggled));

	on_sink_recording_enabled_changed();

	on_filters_changed();
}

void
LogControl::update_button_state()
{
	m_enable_messages_button.set_active(adt::Log::get_message_type_enabled());
	m_enable_function_calls_button.set_active(
	    adt::Log::get_function_type_enabled());
	m_enable_data_button.set_active(adt::Log::get_data_type_enabled());
	m_enable_allocation_button.set_active(adt::Log::get_allocation_type_enabled());
}

void
LogControl::on_sink_recording_enabled_changed()
{
	bool enabled = ApplicationPrivate::get_instance()->get_recording_enabled();
	// Only relevant if change wasn't driven by the button
	if (enabled != m_recording_button.get_active()) {
		m_recording_button.set_active(enabled);
	}
}

void
LogControl::on_filters_changed()
{
	auto filters = adt::Log::get_filters();

	if (filters.empty()) {
		m_enable_filters_button.set_active(false);
		m_enable_filters_button.set_sensitive(false);
	} else {
		if (!m_enable_filters_button.get_sensitive()) {
			m_enable_filters_button.set_sensitive(true);
			// Enable filters when first filter added
			m_enable_filters_button.set_active(true);
		}
	}
}

void
LogControl::on_recording_button_toggled()
{
	ApplicationPrivate::get_instance()->set_recording_enabled(
	    m_recording_button.get_active());
}

void
LogControl::on_auto_update_button_toggled()
{
	ApplicationPrivate::get_instance()->set_auto_update(
	    m_auto_update_button.get_active());
}

void
LogControl::on_enable_tracing_button_toggled()
{
	bool enable_tracing = m_enable_tracing_button.get_active();
	adt::Log::set_trace_enabled(enable_tracing);
}

void
LogControl::on_enable_sync_button_toggled()
{
	bool enable_sync = m_enable_sync_button.get_active();
	adt::Log::set_sync_enabled(enable_sync);
}

void
LogControl::on_enable_properties_button_toggled()
{
	bool enable_properties = m_enable_properties_button.get_active();
	adt::Log::set_properties_enabled(enable_properties);
}

void
LogControl::on_enable_filters_button_toggled()
{
	bool enable_filters = m_enable_filters_button.get_active();
	adt::Log::set_filters_enabled(enable_filters);
}

void
LogControl::on_enable_messages_button_toggled()
{
	bool enable_messages = m_enable_messages_button.get_active();
	adt::Log::set_message_type_enabled(enable_messages);
}

void
LogControl::on_enable_function_calls_button_toggled()
{
	bool enable_function_calls = m_enable_function_calls_button.get_active();
	adt::Log::set_function_type_enabled(enable_function_calls);
}

void
LogControl::on_enable_data_button_toggled()
{
	bool enable_data = m_enable_data_button.get_active();
	adt::Log::set_data_type_enabled(enable_data);
}

void
LogControl::on_enable_allocation_button_toggled()
{
	bool enable_allocation = m_enable_allocation_button.get_active();
	adt::Log::set_allocation_type_enabled(enable_allocation);
}

void
LogControl::on_enable_global_allocation_button_toggled()
{
	bool enable_global_allocation = m_enable_global_allocation_button.get_active();

	if (enable_global_allocation) {
		m_auto_update_button.set_active(false);
	}

	adt::GlobalAllocator::set_logging_enabled(enable_global_allocation);
}
