ClassListView::ClassListView()
    : m_add_filter_button("Add Filter")
{
	m_top_hbox.pack_start(m_add_filter_button, false, false);

	m_add_filter_button.set_sensitive(false);

	pack_start(m_top_hbox, Gtk::PACK_SHRINK);

	m_class_list_store = Gtk::ListStore::create(m_rlcols);

	m_class_tree_view.set_model(m_class_list_store);
	m_class_tree_view.append_column("Class Name", m_rlcols.class_name);
	m_class_tree_view.append_column("Class Size", m_rlcols.class_size);
	m_class_tree_view.append_column("Class Count", m_rlcols.class_count);
	m_class_tree_view.append_column("Class Copied Count",
	                                m_rlcols.class_copied_count);
	m_class_tree_view.set_headers_visible(true);
	m_class_tree_view.set_reorderable(false);
	m_class_tree_view.get_selection()->set_mode(Gtk::SELECTION_SINGLE);

	auto column = m_class_tree_view.get_column(0);
	if (column) {
		column->set_sort_column(m_rlcols.class_name);
	}

	column = m_class_tree_view.get_column(1);
	if (column) {
		column->set_sort_column(m_rlcols.class_size);
	}

	column = m_class_tree_view.get_column(2);
	if (column) {
		column->set_sort_column(m_rlcols.class_count);
	}

	column = m_class_tree_view.get_column(3);
	if (column) {
		column->set_sort_column(m_rlcols.class_copied_count);
	}

	m_class_tree_view.get_selection()->signal_changed().connect(
	    sigc::mem_fun(this, &ClassListView::on_selection_changed));

	m_scrolled_window.add(m_class_tree_view);

	pack_start(m_scrolled_window);

	set_size_request(640, 480);

	ApplicationPrivate::get_instance()->auto_update_connect(
	    sigc::mem_fun(*this, &ClassListView::on_auto_update));

	m_class_tree_view.signal_row_activated().connect(
	    sigc::mem_fun(*this, &ClassListView::on_row_activated));

	m_add_filter_button.signal_pressed().connect(
	    sigc::mem_fun(*this, &ClassListView::on_add_filter_button_pressed));
}

void
ClassListView::on_selection_changed()
{
	Gtk::TreeModel::iterator i = m_class_tree_view.get_selection()->get_selected();

	if (i) {
		m_selected_class_tracker = (*i)[m_rlcols.class_tracker];
		m_add_filter_button.set_sensitive(true);
	} else {
		m_add_filter_button.set_sensitive(false);
	}
}

void
ClassListView::on_add_filter_button_pressed()
{
	add_filter_from_selected();
}

void
ClassListView::on_row_activated(const Gtk::TreeModel::Path&,
                                Gtk::TreeViewColumn*)
{
	add_filter_from_selected();
}

void
ClassListView::add_filter_from_selected()
{
#if 0
	FilterDialog dialog("Add Filter");

	dialog.set_class_info(m_selected_class_info);

	auto const response = dialog.run();

	if (response != Gtk::RESPONSE_OK) {
		return;
	}

	ApplicationPrivate::get_instance()->add_filter(dialog.get_filter());
#endif
}

void
ClassListView::on_auto_update()
{
	refresh();
}

void
ClassListView::refresh()
{
	auto const class_trackers = adt::ClassTrackerManager::get_class_trackers();

	// Add all new classes not already represented
	for (auto const& class_tracker : class_trackers) {
		bool found = false;

		for (auto const& iter : m_class_list_store->children()) {
			auto const row = *iter;
			adt::ClassTracker* c_tracker = row[m_rlcols.class_tracker];
			if (c_tracker == class_tracker) {
				found = true;
				// update
				row[m_rlcols.class_name] = class_tracker->get_tracked_class_name();
				row[m_rlcols.class_size] = class_tracker->get_tracked_class_size();
				row[m_rlcols.class_count] = class_tracker->get_tracked_class_count();
				row[m_rlcols.class_copied_count] =
				    class_tracker->get_tracked_class_copied_count();
				break;
			}
		}

		if (!found) {
			Gtk::TreeModel::Row newrow = *(m_class_list_store)->append();
			newrow[m_rlcols.class_name] = class_tracker->get_tracked_class_name();
			newrow[m_rlcols.class_size] = class_tracker->get_tracked_class_size();
			newrow[m_rlcols.class_count] = class_tracker->get_tracked_class_count();
			newrow[m_rlcols.class_copied_count] =
			    class_tracker->get_tracked_class_copied_count();
			newrow[m_rlcols.class_tracker] = class_tracker;
		}
	}

	// Remove all classes that no longer exist
	for (auto iter = m_class_list_store->children().begin();
	     iter != m_class_list_store->children().end();) {
		auto const row = *iter;
		adt::ClassTracker* c_tracker = row[m_rlcols.class_tracker];
		bool found = false;

		for (auto const& class_tracker : class_trackers) {
			if (c_tracker == class_tracker) {
				found = true;
				break;
			}
		}

		if (!found) {
			iter = m_class_list_store->erase(iter);
		} else {
			iter++;
		}
	}
}

void
ClassListView::clear()
{
	m_class_list_store->clear();
}
