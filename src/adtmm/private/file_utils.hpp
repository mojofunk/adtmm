bool
find_file_in_search_path(std::vector<std::string> const& paths,
                         std::string const& filename,
                         std::string& absolute_file_path);
