class DeveloperWindowPrivate : public Gtk::VBox
{
public:
	DeveloperWindowPrivate();

public: // Methods
	void on_selected_log_record_changed();

public: // Members
	LogControl log_control;
	LogCategoryControl log_category_control;
	LogRecordListView log_record_list_view;
	SourceView source_view;
	FilterListView filter_view;
	ThreadListView thread_view;
	ClassListView class_list_view;
	TraceView trace_view;
	PropertyListView property_list_view;

private: // Members
	Gtk::HBox m_top_hbox;

	Gtk::HPaned m_top_hpaned;
	Gtk::HPaned m_bottom_hpaned;
	Gtk::VPaned m_vpaned;

	Gtk::Notebook m_bottom_left_notebook;
	Gtk::Notebook m_bottom_right_notebook;
};
