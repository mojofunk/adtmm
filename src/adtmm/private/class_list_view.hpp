class ClassListView : public Gtk::VBox
{
public:
	ClassListView();

	struct ClassListColumns : public Gtk::TreeModel::ColumnRecord {
	public:
		ClassListColumns()
		{
			add(class_name);
			add(class_size);
			add(class_count);
			add(class_copied_count);
			add(class_tracker);
		}
		Gtk::TreeModelColumn<std::string> class_name;
		Gtk::TreeModelColumn<std::size_t> class_size;
		Gtk::TreeModelColumn<uint32_t> class_count;
		Gtk::TreeModelColumn<uint32_t> class_copied_count;
		Gtk::TreeModelColumn<adt::ClassTracker*> class_tracker;
	};

private: // Methods
	void refresh();

	void clear();

	void on_selection_changed();

	void on_auto_update();

	void on_row_activated(const Gtk::TreeModel::Path&, Gtk::TreeViewColumn*);

	void on_add_filter_button_pressed();

	void add_filter_from_selected();

private:
	Gtk::HBox m_top_hbox;

	Gtk::Button m_add_filter_button;

	Gtk::ScrolledWindow m_scrolled_window;

	Glib::RefPtr<Gtk::ListStore> m_class_list_store;
	ClassListColumns m_rlcols;
	Gtk::TreeView m_class_tree_view;

	adt::ClassTracker* m_selected_class_tracker;
};
