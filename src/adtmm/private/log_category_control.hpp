class LogCategoryControl : public Gtk::VBox
{
public:
	LogCategoryControl();

	void refresh();

private: // Classes
	struct LogCategoryListColumns : public Gtk::TreeModel::ColumnRecord {
		LogCategoryListColumns()
		{
			add(name);
			add(enabled);
			add(category);
			add(visible);
		}
		Gtk::TreeModelColumn<std::string> name;
		Gtk::TreeModelColumn<bool> enabled;
		Gtk::TreeModelColumn<std::shared_ptr<LogCategory>> category;
		Gtk::TreeModelColumn<bool> visible;
	};

private: // Methods
	void category_enabled_toggled(const Glib::ustring& path);

	bool is_row_visible(const Gtk::TreeModel::const_iterator& iter);

private: // Event handlers
	void on_search_entry_changed();
	void on_search_entry_activated();
	bool on_search_entry_key_press_event(GdkEventKey*);

	void on_clear_button_pressed();

private: // Members
	Gtk::HBox m_search_box;

#ifdef USE_GTK3
	Gtk::SearchEntry m_search_entry;
#else
	Gtk::Entry m_search_entry;
	Gtk::Button m_clear_button;
#endif

	Glib::RefPtr<Gtk::ListStore> m_category_list_store;
	LogCategoryListColumns m_llcols;
	Glib::RefPtr<Gtk::TreeModelFilter> m_tree_model_filter;

	Gtk::TreeView category_tree_view;

	Gtk::ScrolledWindow m_scrolled_window;

	PresetControlBox m_preset_control_box;
};
