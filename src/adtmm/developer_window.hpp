class DeveloperWindowPrivate;

class DeveloperWindow : public Gtk::Window
{
public:
	DeveloperWindow();

private:
	std::unique_ptr<DeveloperWindowPrivate> d_ptr;
};
