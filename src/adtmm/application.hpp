class DeveloperWindow;

class Application
{
public:
	/**
	 * Use this constructor if using external option parsing and setting
	 * LibraryOptions manually before creating Application instance.
	 */
	Application();

	/**
	 * Let Application parse the command line and remove relevant options
	 */
	Application(int* argc, char*** argv);

	~Application();

public:
	static DeveloperWindow* get_developer_window();

public:
	static void set_source_search_paths(std::vector<std::string> const& paths);

	static std::vector<std::string> get_source_search_paths();
};
