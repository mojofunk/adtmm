#ifndef ADTMM_H
#define ADTMM_H

#include "adtmm/header_includes.hpp"

namespace adtmm {

#include "adtmm/application.hpp"
#include "adtmm/developer_window.hpp"
#include "adtmm/library_options.hpp"

} // namespace adtmm

#endif // ADTMM_H
