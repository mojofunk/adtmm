#include "adtmm.hpp"

#include "adtmm-private.hpp"

namespace adtmm {

#include "adtmm-private.cpp"

#include "adtmm/application.cpp"
#include "adtmm/developer_window.cpp"
#include "adtmm/library_options.cpp"

} // namespace adtmm
