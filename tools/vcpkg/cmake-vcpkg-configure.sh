#! /bin/bash

. cmake-vcpkg-env.sh

rm -rf $BUILD_DIR

mkdir $BUILD_DIR && cd $BUILD_DIR

# Copying the ninja executable is necessary on x86 as ninja upstream don't
# provide a x86 executable. So it will need to be built and copied to vcpkg
# root. Otherwise ninja should be in PATH or ...
cp "$NINJA_PATH/ninja.exe" . || exit

CMAKE_CMD="cmake -GNinja -DCMAKE_C_COMPILER=\"cl.exe\" -DCMAKE_CXX_COMPILER=\"cl.exe\" -DBUILD_SHARED_LIBS=1 -DCMAKE_TOOLCHAIN_FILE=$VCPKG_TOOLCHAIN_FILE $TOP_DIR"

call_in_vsdevcmd "$CMAKE_CMD"
