#! /bin/bash

# To install to a temporary directory for testing etc use
# $ DESTDIR=./tmp ./cmake-install.sh
# otherwise use sudo to install system wide
BUILD_DIR=cmake-build-gtk2

cd $BUILD_DIR && ninja install
