#! /bin/bash

BUILD_DIR=cmake-build-gtk2

rm -rf $BUILD_DIR

mkdir $BUILD_DIR && cd $BUILD_DIR

#BUILD_TYPE="-DCMAKE_BUILD_TYPE=Debug"

cmake -GNinja $BUILD_TYPE "-DENABLE_GTK3=OFF" "-DBUILD_SHARED_LIBS=1" "-DCMAKE_INSTALL_PREFIX:PATH=/usr" ../../../ && ninja -v
