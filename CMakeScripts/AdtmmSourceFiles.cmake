file(GLOB ADTMM_HEADER "src/adtmm.hpp")
file(GLOB ADTMM_SUBHEADERS "src/adtmm/*.hpp")

file(GLOB ADTMM_PRIVATE_HEADER "src/adtmm-private.hpp")
file(GLOB ADTMM_PRIVATE_SUBHEADERS "src/adtmm/private/*.hpp")

set(ADTMM_SOURCES "src/adtmm.cpp")
