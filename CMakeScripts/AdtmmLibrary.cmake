if(ENABLE_GTK3)
    set(GTK_VERSION 3)
else()
    set(GTK_VERSION 2)
endif()

set(ADTMM_TARGET adtmm-${GTK_VERSION})

add_library(${ADTMM_TARGET} ${ADTMM_SOURCES})

set_target_properties(${ADTMM_TARGET} PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    VERSION ${ADTMM_VERSION}
    SOVERSION ${ADTMM_VERSION_MAJOR}
)

if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4250")
endif()

target_include_directories(${ADTMM_TARGET} PUBLIC src)

if(BUILD_SHARED_LIBS)
    target_compile_definitions(${ADTMM_TARGET} PRIVATE "ADTMM_BUILDING_DLL")
    if(MSVC)
        message("MSVC detected, exporting all symbols from DLL")
        set_target_properties(${ADTMM_TARGET} PROPERTIES
            WINDOWS_EXPORT_ALL_SYMBOLS true
        )
    endif()
else()
    target_compile_definitions(${ADTMM_TARGET} PRIVATE "ADTMM_STATIC")
endif()


target_compile_definitions(${ADTMM_TARGET} PRIVATE "USE_GTK${GTK_VERSION}")

target_include_directories(${ADTMM_TARGET} PUBLIC ${ADTMM_DEPS_INCLUDE_DIRS})

target_link_libraries(${ADTMM_TARGET} PUBLIC ${ADTMM_DEPS_LIBRARIES})

if(ENABLE_GTK3)
    if(GSVMM3_FOUND)
        target_compile_definitions(${ADTMM_TARGET} PRIVATE "HAVE_GSV3=1")
        target_include_directories(${ADTMM_TARGET} PRIVATE
                                   ${GSVMM3_INCLUDE_DIRS})
        target_link_libraries(${ADTMM_TARGET} PRIVATE ${GSVMM3_LIBRARIES})
    endif()
else()
    if(GSVMM2_FOUND)
        target_compile_definitions(${ADTMM_TARGET} PRIVATE "HAVE_GSV2=1")
        target_include_directories(${ADTMM_TARGET} PRIVATE
                                   ${GSVMM2_INCLUDE_DIRS})
        target_link_libraries(${ADTMM_TARGET} PRIVATE ${GSVMM2_LIBRARIES})
    endif()
endif()
