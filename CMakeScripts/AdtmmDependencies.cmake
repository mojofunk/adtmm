find_package(PkgConfig REQUIRED)

if(ENABLE_GTK3)
    set(GTKMM_VERSION 3.0)
    set(GTKSOURCEVIEWMM_VERSION 3.0)
else()
    set(GTKMM_VERSION 2.4)
    set(GTKSOURCEVIEWMM_VERSION 2.0)
endif()

pkg_check_modules(ADTMM_DEPS REQUIRED adt-0 gtkmm-${GTKMM_VERSION})
pkg_check_modules(GSVMM2 gtksourceviewmm-${GTKSOURCEVIEWMM_VERSION})
