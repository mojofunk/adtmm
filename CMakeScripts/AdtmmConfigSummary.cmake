message("------------------------------------------------------------------------")
message("")
message("Configuration Summary")
message("")

# project info
message("ADTMM_VERSION:           ${ADTMM_VERSION}")
message("")

# cmake info
message("CMAKE_BINARY_DIR:        ${CMAKE_BINARY_DIR}")
message("CMAKE_INSTALL_PREFIX:    ${CMAKE_INSTALL_PREFIX}")
message("CMAKE_SYSTEM_NAME:       ${CMAKE_SYSTEM_NAME}")
message("CMAKE_SYSTEM_VERSION:    ${CMAKE_SYSTEM_VERSION}")
message("CMAKE_SYSTEM_PROCESSOR:  ${CMAKE_SYSTEM_PROCESSOR}")
message("CMAKE_C_COMPILER:        ${CMAKE_C_COMPILER}")
message("CMAKE_CXX_COMPILER:      ${CMAKE_CXX_COMPILER}")
message("CMAKE_BUILD_TYPE:        ${CMAKE_BUILD_TYPE}")
message("")

# options
message("BUILD_SHARED_LIBS:       ${BUILD_SHARED_LIBS}")
message("INSTALL_HEADERS:         ${INSTALL_HEADERS}")
message("ENABLE_GTK3:             ${ENABLE_GTK3}")

if(ENABLE_GTK3)
    message("Using gtksourceviewmm-3.0: ${GSVMM3_FOUND}")
else()
    message("Using gtksourceviewmm-2.0: ${GSVMM2_FOUND}")
endif()

message("")
message("------------------------------------------------------------------------")
