if(ENABLE_GTK3)
    set(GTKMM3_EXAMPLE hello_world_gtkmm3_example)

    add_executable(${GTKMM3_EXAMPLE} "examples/${GTKMM3_EXAMPLE}.cpp")

    set(GTKMM3_EXAMPLE_DEPS_LIBRARIES
        ${ADTMM_TARGET}
        ${GIOMM_LIBRARIES}
    )

    target_link_libraries(${GTKMM3_EXAMPLE} PUBLIC ${GTKMM3_EXAMPLE_DEPS_LIBRARIES})
else()
    set(GTKMM2_EXAMPLE hello_world_gtkmm2_example)

    add_executable(${GTKMM2_EXAMPLE} "examples/${GTKMM2_EXAMPLE}.cpp")

    set(GTKMM2_EXAMPLE_DEPS_LIBRARIES
        ${ADTMM_TARGET}
        ${GIOMM_LIBRARIES}
    )

    target_link_libraries(${GTKMM2_EXAMPLE} PUBLIC ${GTKMM2_EXAMPLE_DEPS_LIBRARIES})
endif()
